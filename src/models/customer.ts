export default class Customer {

    public id: number;
    public name: string;

    public constructor(id?: number, name?: string) {
        this.id = id ? id : 0;
        this.name = name ? name : "";
    }

}