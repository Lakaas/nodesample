/**
 * Created by JP on 03/22/18
 */

import env from "./config/environment";
import * as server from "./config/server";
import * as bodyparser from "body-parser";
import * as cors from "cors";
import api from "./api/api";
import chalk from "chalk";

// middlewares

server.app.use(bodyparser.urlencoded({ extended: true }));
server.app.use(bodyparser.json());
server.app.use(cors());

// instantiating apis

api();

// handling default routes

server.app.use("/", server.router);

// serving app

server.app.listen(env.port, err => {
    if(err) {
        console.log(
            chalk.bold.red("->"),
            chalk.red(err),
            chalk.bold.red("<-")
        );
    }
    else {
        console.log(
            chalk.bold.magenta("->"),
            chalk.green(`${env.appName} - Server listening on port`), 
            chalk.bold.yellow(`${env.port}`), 
            chalk.bold.magenta("<-")
        );
    }
})