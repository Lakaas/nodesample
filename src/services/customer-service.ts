import { List } from "linqts";
import Customer from "../models/customer";
import { customerProvider } from "../providers/providers";

export default class CustomerService {

    getAll(): Array<Customer> {
        return customerProvider.getAll().ToArray();
    }

    getById(id: number): Customer {
        return customerProvider.getById(id);
    }

    getByName(name: string): Array<Customer> {
        return customerProvider.getByName(name).ToArray();
    }

    create(customer: Customer): void {
        customerProvider.create(customer);
    }

    update(customer: Customer): void {
        customerProvider.update(customer);
    }
    
    delete(customer: Customer): void {
        customerProvider.delete(customer);
    }

    toCustomer(json: any): Customer {
        let id: number = json.id;
        let name: string = json.name;
        return new Customer(json.id, json.name);
    }
    
}