
const environment = {
    appName: "MyApp" as string,
    port: 8085 as number
};

export default environment;