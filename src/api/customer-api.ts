import { router } from "../config/server";


const root: string = "/api/Customer";
import { customerService } from "../services/services";

export default class CustomerApi {

    constructor() {
        this.defineApiMethods();
    }

    private defineApiMethods() {
        
        router.get(root + "/GetAll", async (req, res) => {
            res.json(await customerService.getAll());
        });

        router.get(root + "/GetById", async (req, res) => {
            let id: number = req.query.id ? parseInt(req.query.id) : 0;
            res.json(await customerService.getById(id));
        });

        router.get(root + "/GetByName", async (req, res) => {
            let name: string = req.query.name ? req.query.name : "";
            res.json(await customerService.getByName(req.query.name));
        });

        router.post(root + "/Create", async (req, res) => {
            res.json(await customerService.create(customerService.toCustomer(req.body)));
        });

        router.post(root + "/Update", async (req, res) => {
            res.json(await customerService.update(customerService.toCustomer(req.body)));
        });

        router.post(root + "/Delete", async (req, res) => {
            res.json(await customerService.delete(customerService.toCustomer(req.body)));
        });
    }

}