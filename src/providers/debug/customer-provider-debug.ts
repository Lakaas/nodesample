import { List } from "linqts";
import Customer from "../../models/customer";


export default class CustomerProviderDebug {

    getAll(): List<Customer> {
        return debugData;
    }

    getById(id: number): Customer {
        return debugData
        .FirstOrDefault(c => c.id === id);
    }

    getByName(name: string): List<Customer> {
        return debugData
        .Where(c => c.name.includes(name));
    }

    create(customer: Customer): void {
        debugData.Add(customer);
    }

    update(customer: Customer): void {
        let toUpdate: Customer = debugData.FirstOrDefault(c => c.id === customer.id);
        if(toUpdate) {
            toUpdate = customer;
        }
    }
    
    delete(customer: Customer): void {
        debugData.Remove(debugData.FirstOrDefault(c => c.id === customer.id));
    }
}


let debugData: List<Customer> = new List<Customer>([
    new Customer(1, "John Doe"),
    new Customer(2, "Foo Bar"),
    new Customer(3, "Rebecca Johnson"),
    new Customer(4, "Jack Jones"),
    new Customer(5, "Martin Dupont")
]);